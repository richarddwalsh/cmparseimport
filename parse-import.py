__author__ = "Richard Walsh"
__email__ = "richard@thechirp.ca"
__copyright__ = "Copyright 2016, Chirp Media, INC"
__license__ = "MIT"

import json, httplib, os
import datetime

from optparse import OptionParser

import json, httplib, os
import datetime

from optparse import OptionParser

from pprint import pprint


def main():
    parser = OptionParser()
    parser.add_option("-a", "--parseappid", dest="parse_app_id",
                      help="Parse App ID",)
    parser.add_option("-m", "--parsemaster", dest="parse_master_key",
                      help="Parse Master Key",)
    parser.add_option("-p", "--parseclassname", dest="parse_class_name",
                      help="Parse Class Name",)
    parser.add_option("-s", "--parseserverurl", dest="parse_server_url",
                      help="Parse Server URL",)
    parser.add_option("-d", "--jsonfile", dest="json_file",
                      help="Your JSON Data File")
    parser.add_option("-u", "--method", dest="method",
                      help="Your Import or Update Method")

    (options, args) = parser.parse_args()

    if options.parse_app_id:
        PARSE_APP_ID = options.parse_app_id
    else:
        assert False, 'PARSE_APP_ID is blank!  Visit https://www.parse.com to obtain your keys.'

    if options.parse_master_key:
        PARSE_MASTER_KEY = options.parse_master_key
    else:
        assert False, 'PARSE_MASTER_KEY is blank!  Visit https://www.parse.com to obtain your keys.'

    if options.parse_class_name:
        PARSE_CLASS_NAME = options.parse_class_name
    else:
        assert False, 'PARSE_CLASS_NAME is blank!  You must define the className of the data.'

    if options.parse_server_url:
        PARSE_SERVER_URL = options.parse_server_url
    else:
        assert False, 'PARSE_SERVER_URL is blank!  You must set the server url.'

    if options.json_file:
        JSON_FILE = options.json_file
    else:
        assert False, 'JSON_FILE is blank!  You must define the json data file source.'

    if options.parse_server_url:
        METHOD = options.method
    else:
        assert False, 'METHOD is blank!  You must set the method to PUT or POST.'

    json_data = json.loads(open(JSON_FILE).read())
    result_count = len(json_data["results"])
    print "Processing %s Deals" % (result_count)

    connection = httplib.HTTPSConnection("PARSE_SERVER_URL", 443)
    connection.connect()

    for obj in json_data["results"]:
        object_id = obj["objectId"]

        endpoint = "/parse/classes/%s/%s" % (PARSE_CLASS_NAME, object_id)

        body = json.dumps(obj)

        connection.request(METHOD, endpoint, body, {
            "X-Parse-Application-Id": PARSE_APP_ID,
            "X-Parse-Master-Key": PARSE_MASTER_KEY,
            "Content-Type": "application/json",
            "params": {
                "lastActivityAt": obj["lastActivityAt"]["iso"]
            }
        })

        result = json.loads(connection.getresponse().read())
        print result


if __name__ == '__main__':
    main()
