# CMParseImport
Parse Importer for JSON data on a self hosted Parse-Server, or the api.parse.com server.

A python script to import a json data file to your self hosted parse-server, or the api.parse.com server.

*There is a current bug with the declaration of the PARSE_SERVER_URL, in the meantime, edit the python file in your text editor and replace this line:

```
connection = httplib.HTTPSConnection(PARSE_SERVER_URL, 443)
connection.connect()
```

With:

```
connection = httplib.HTTPSConnection(XXX, 443) // XXX = your parse-server url 
connection.connect()
```*

## Usage

```
$ python parse-import.py -a <PARSE_APP_ID> -m <PARSE_MASTER_KEY>" -p <PARSE_CLASS_NAME> -s <PARSE_SERVER_URL> -d <JSON_FILE>
```

Your data output will read:

```
Processing 3 <PARSE_CLASS_NAME>
{u'updatedAt': u'2016-08-24T08:23:46.936Z'}
{u'updatedAt': u'2016-08-24T08:23:46.936Z'}
{u'updatedAt': u'2016-08-24T08:23:46.936Z'}
Finished Processing 3 <PARSE_CLASS_NAME>
```

NOTE:

#### `-a` `PARSE_APP_ID`
Your Parse Application ID

#### `-m` `PARSE_MASTER_KEY`
Your Parse Master Key - **NEVER GIVE THIS TO ANYONE AND DO NOT SAVE IT TO YOUR REPO**

#### `-p` `PARSE_CLASS_NAME`
The name of the class your are importing to

#### `-s` `PARSE_SERVER_URL`
The url of your selfhosted parse server. - i.e. http://server.herokuapp.com/ **DO NOT INCLUDE "PARSE" IN URL**